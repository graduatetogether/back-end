package majibackend.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Student {
//    @Id
//    @GeneratedValue(strategy=GenerationType.AUTO)




    //    @Id
//    @GeneratedValue(strategy= GenerationType.AUTO)
//    @Id @GeneratedValue(generator="system-uuid")
//    @GenericGenerator(name="system-uuid", strategy = "uuid")
//    private String studentUUID;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "studentUUID", updatable = false, nullable = false)
    private Long studentUUID;

    private String studentID;

    private String studentFname;
    private String studentLname;


    private String password;


    private String authKey;



    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "student_section", joinColumns =  @JoinColumn(name = "studentUUID"),
            inverseJoinColumns = { @JoinColumn(name = "sectionUUID")})
    public Set<Section> sections;




    public Student(String studentID, String studentFname, String studentLname, String password, String authKey) {
        this.studentID = studentID;

        this.studentFname = studentFname;
        this.studentLname = studentLname;
        this.password = password;
        this.authKey = authKey;
    }

    public Student(Long studentUUID, Set<Section> sections) {
        this.studentUUID = studentUUID;
        this.sections = sections;
    }
    public Student(){}

    public Long getStudentUUID() {
        return studentUUID;
    }

    public void setStudentUUID(Long studentUUID) {
        this.studentUUID = studentUUID;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentFname() {
        return studentFname;
    }

    public void setStudentFname(String studentFname) {
        this.studentFname = studentFname;
    }

    public String getStudentLname() {
        return studentLname;
    }

    public void setStudentLname(String studentLname) {
        this.studentLname = studentLname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public Set<Section> getSections() {
        return sections;
    }

    public void setSections(Set<Section> sections) {
        this.sections = sections;
    }

}

