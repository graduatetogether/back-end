package majibackend.model;

import javax.persistence.*;


@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "courseUUID", updatable = false, nullable = false)
    private Long courseUUID;

    private String courseID;

    private String courseName;


    private String courseDescription;

    private String courseTerm;

    private Long teacherUUID;

    public Course(){

    }

    public Course(String courseID, String courseName, String courseDescription, String courseTerm, Long teacherUUID) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.courseTerm = courseTerm;
        this.teacherUUID = teacherUUID;

    }


    public Long getCourseUUID() {
        return courseUUID;
    }

    public void setCourseUUID(Long courseUUID) {
        this.courseUUID = courseUUID;
    }

    public String getCourseID() {
        return courseID;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }


    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescriotion(String courseDescriotion) {
        this.courseDescription = courseDescriotion;
    }

    public String getCourseTerm() {
        return courseTerm;
    }

    public void setCourseTerm(String courseTerm) {
        this.courseTerm = courseTerm;
    }

    public Long getTeacherUUID() {
        return teacherUUID;
    }

    public void setTeacherUUID(Long teacherUUID) {
        this.teacherUUID = teacherUUID;
    }






}
