package majibackend.model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;


@Entity
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attendanceUUID", updatable = false, nullable = false)
    private Long attendanceUUID;

    private Date date;
    private Time time;
    private String status;

    private Long studentUUID;
    private Long sectionUUID;




    private String installationID;

    public Attendance(Date date, Time time, String status, Long studentUUID, Long sectionUUID, String installationID) {
        this.date = date;
        this.time = time;
        this.status = status;
        this.studentUUID = studentUUID;
        this.sectionUUID = sectionUUID;
        this.installationID = installationID;
    }

    public Attendance(){}

    public Long getStudentUUID() {
        return studentUUID;
    }

    public Long getSectionUUID() {
        return sectionUUID;
    }

    public Date getDate() {
        return date;
    }

    public Time getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInstallationID() {
        return installationID;
    }

    public void setInstallationID(String installationID) {
        this.installationID = installationID;
    }

}

