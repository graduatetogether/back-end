package majibackend.model;



import javax.persistence.*;
import javax.validation.constraints.Email;


@Entity
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "teacherUUID", updatable = false, nullable = false)
    private Long teacherUUID;

    @Email(message = "Email should be valid")
    private String teacherEmail;

    private String teacherFname;

    private String teacherLname;

    private String password;

    private String authKey;


    public Teacher(String teacherEmail, String teacherFname, String teacherLname, String password, String authKey) {
        this.teacherEmail = teacherEmail;

        this.teacherFname = teacherFname;
        this.teacherLname = teacherLname;
        this.password = password;
        this.authKey = authKey;
    }

    public Teacher(){}

    public Long getTeacherUUID() {
        return teacherUUID;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public String getPassword() {
        return password;
    }

    public String getTeacherFname() {
        return teacherFname;
    }

    public String getTeacherLname() {
        return teacherLname;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTeacherFname(String teacherFname) {
        this.teacherFname = teacherFname;
    }

    public void setTeacherLname(String teacherLname) {
        this.teacherLname = teacherLname;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }
}
