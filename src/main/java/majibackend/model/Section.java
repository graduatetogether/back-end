package majibackend.model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Set;

@Entity
public class Section {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sectionUUID", updatable = false, nullable = false)
    private Long sectionUUID;

    private String sectionDateAndTime;

    private Long sectionNumber;

    private String enrollID; // 6, numeric and number

    private String sectionRoom;

    private Time startGenQRTime;

    private Date startGenQRDate;

    private Long courseUUID;

    private String color;


    @ManyToMany(mappedBy = "sections")
    private Set<Student> students;



//    @ElementCollection(targetClass=Integer.class)
//    private Set<Student> students;

    public Section(String sectionDateAndTime, Long sectionNumber, String enrollID, String sectionRoom, Long courseUUID, String color) {
        this.sectionDateAndTime = sectionDateAndTime;
        this.sectionNumber = sectionNumber;
        this.enrollID = enrollID;
        this.sectionRoom = sectionRoom;
        this.courseUUID = courseUUID;
        this.startGenQRDate = null;
        this.startGenQRTime = null;
        this.color = color;
    }

//    @ManyToMany(mappedBy = "sections")
//    private Set<Student> students;
    public Section(){}

    public Section(Long sectionUUID) {
        this.sectionUUID = sectionUUID;
    }

    public Long getSectionUUID() {
        return sectionUUID;
    }

    public void setSectionUUID(Long sectionUUID) {
        this.sectionUUID = sectionUUID;
    }

    public String getSectionDateAndTime() {
        return sectionDateAndTime;
    }

    public void setSectionDateAndTime(String sectionDateAndTime) {
        this.sectionDateAndTime = sectionDateAndTime;
    }

    public Long getSectionNumber() {
        return sectionNumber;
    }

    public void setSectionNumber(Long sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    public String getEnrollID() {
        return enrollID;
    }

    public void setEnrollID(String enrollID) {
        this.enrollID = enrollID;
    }

    public String getRoom() {
        return sectionRoom;
    }

    public void setRoom(String sectionRoom) {
        this.sectionRoom = sectionRoom;
    }

    public Long getCourseUUID() {
        return courseUUID;
    }

    public void setCourseUUID(Long courseUUID) {
        this.courseUUID = courseUUID;
    }

    public void setStartGenQRDate(Date startGenQRDate) {
        this.startGenQRDate = startGenQRDate;
    }

    public void setStartGenQRTime(Time startGenQRTime) {
        this.startGenQRTime = startGenQRTime;
    }

    public Time getStartGenQRTime() {
        return startGenQRTime;
    }

    public Date getStartGenQRDate() {
        return startGenQRDate;
    }
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

//    public Set<Student> getStudents() {
//        return students;
//    }
//
//    public void setStudents(Set<Student> students) {
//        this.students = students;
//    }
}
