package majibackend.controller;


import majibackend.model.Section;
import majibackend.model.Student;
import majibackend.service.AuthenticationService;
import majibackend.repository.SectionRepository;
import majibackend.repository.StudentRepository;
import majibackend.service.SectionService;
import majibackend.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class StudentController {


    @Autowired
    private StudentService studentService;

    @Autowired
    private AuthenticationService authenticationService;


    @Autowired
    private SectionService sectionService;


    @GetMapping("/all-student")
    public List<Student> getAll() {
        return studentService.getAllStudent();
    }

    @GetMapping("/student-info")
    public ResponseEntity getStudentInfo(@RequestParam("authkey") String authKey) {

        if (authenticationService.studentExistByAuthKey(authKey)) {
            try {
                Map<String, Object> student = studentService.getStudentInfo(authKey);
                return ResponseEntity.ok().body(student);

            }catch (Exception e){
                return ResponseEntity.badRequest().body(e);
            }


        }
        return ResponseEntity.badRequest().body("Please log in");
    }

    @PostMapping("/student-register")
    public ResponseEntity studentRegister(@RequestParam("studentid") String studentID,
                                          @RequestParam("studentfname") String studentFname,
                                          @RequestParam("studentlname") String studentLname,
                                          @RequestParam("password") String password) {


        if (studentService.studentExist(studentID)) { // if the id does not exist --> can register

            try {
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                String authKey = UUID.randomUUID().toString();

                Student student = new Student(studentID, studentFname, studentLname, passwordEncoder.encode(password), authKey);
                studentService.saveStudent(student);
                return ResponseEntity.ok("200");

            }catch (Exception e) {
                return ResponseEntity.badRequest().body(e);
            }

        }
        return ResponseEntity.badRequest().body("Student ID already exist");
    }

    @PostMapping("/student-login")
    public ResponseEntity studentLogin(@RequestParam("studentid") String studentID,
                                       @RequestParam("password") String password) {

        if (!studentService.studentExist(studentID)) { // if the studentID exist
            try {
                Student student = studentService.getStudentByID(studentID);

                boolean passwordMatch = studentService.checkPasswordMatch(student, password);

                if (passwordMatch) {
                    HashMap<String, String> info = new HashMap<>();
                    info.put("authKey", student.getAuthKey());
                    return ResponseEntity.ok().body(info);
                }

                return ResponseEntity.badRequest().body("Password mismatch");

            }catch (Exception e) {
                return ResponseEntity.badRequest().body(e);

            }


        }
        return ResponseEntity.badRequest().body("Student ID does not exist");

    }

    @PostMapping("/student-enroll")
    public ResponseEntity studentEnroll(@RequestParam("enrollid") String enrollID,
                                        @RequestParam("authkey") String authKey) {

        if (authenticationService.studentExistByAuthKey(authKey)) {
            try {

                Section section = sectionService.getSectionByEnrollID(enrollID);
                Student student = studentService.getStudentByAuthKey(authKey);

                String response = studentService.studentEnroll(section, student);


                if (response == "200"){
                    return ResponseEntity.ok("200");
                }else{
                    return ResponseEntity.badRequest().body(response);
                }
            }catch (Exception e) {
                return ResponseEntity.badRequest().body(e);
            }
        }

        return ResponseEntity.badRequest().body("Please log in");
    }









}
