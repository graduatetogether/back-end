package majibackend.controller;


import majibackend.model.Course;
import majibackend.model.Teacher;
import majibackend.service.AuthenticationService;
import majibackend.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class TeacherController {


    @Autowired
    private TeacherService teacherService;

    @Autowired
    private AuthenticationService authenticationService;


    @GetMapping("/all-teacher")
    public List<Teacher> getAll() {
        return teacherService.getAllTeacher();
    }

    @GetMapping("/all-courses-by-teacher")
    public ResponseEntity getCourseByTeacher(@RequestParam("authkey") String authKey) {
        if (authenticationService.teacherExistByAuthKey(authKey)) {
            try {

                List<Course> lst = teacherService.getAllCourseByTeacher(authKey);

                return ResponseEntity.ok().body(lst);

            }catch (Exception e) {

                return ResponseEntity.badRequest().body(e);
            }




        }
        return ResponseEntity.badRequest().body("Please log in");
    }

    @GetMapping("/teacher-info")
    public ResponseEntity getTeacherInfo(@RequestParam("authkey") String authKey) {

        if (authenticationService.teacherExistByAuthKey(authKey)) {
            try {
                Teacher teacher = teacherService.getTeacherFromAuthKey(authKey);
                return ResponseEntity.ok().body(teacher);

            }catch (Exception e){
                return ResponseEntity.badRequest().body(e);
            }


        }
        return ResponseEntity.badRequest().body("Please log in");
    }

    @PostMapping("/teacher-register")
    public ResponseEntity teacherRegister(@RequestParam("teacheremail") String teacheremail,
                                          @RequestParam("teacherfname") String teacherfname,
                                          @RequestParam("teacherlname") String teacherlname,
                                          @RequestParam("password") String password) {

        if (teacherService.teacherExistByEmail(teacheremail)) { // if the id does not exist --> can register
            try {


                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                String authKey = UUID.randomUUID().toString();

                Teacher teacher = new Teacher(teacheremail, teacherfname, teacherlname, passwordEncoder.encode(password), authKey);
                teacherService.saveTeacher(teacher);
                return ResponseEntity.ok("200");

            }catch (ConstraintViolationException e){
                return ResponseEntity.badRequest().body("email is not valid");
            }
        }

        return ResponseEntity.badRequest().body("email already exist");

    }

    @PostMapping("/teacher-login")
    public ResponseEntity teacherLogin(@RequestParam("teacheremail") String teacherEmail,
                                       @RequestParam("password") String password) {

        if (!teacherService.teacherExistByEmail(teacherEmail)) { // if teacher exist --> can login

            try {

                Teacher teacher = teacherService.getTeacherFromEmail(teacherEmail);

                boolean passwordMatch = teacherService.checkPasswordMatch(teacher, password);



                if (passwordMatch) {

                    HashMap<String, String> info = new HashMap<>();
                    info.put("authKey", teacher.getAuthKey());
                    return ResponseEntity.ok().body(info);
                }
                return ResponseEntity.badRequest().body("Password mismatch");


            }catch (Exception e) {
                return ResponseEntity.badRequest().body(e);
            }


        }
        return ResponseEntity.badRequest().body("Email does not exist");

    }
}
