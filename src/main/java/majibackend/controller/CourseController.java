package majibackend.controller;


import majibackend.model.Course;
import majibackend.model.Teacher;
import majibackend.repository.CourseRepository;
import majibackend.repository.TeacherRepository;
import majibackend.service.AuthenticationService;
import majibackend.service.CourseService;
import majibackend.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CourseController {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    CourseService courseService;

    @Autowired
    TeacherService teacherService;

    @Autowired
    AuthenticationService authenticationService;

    @GetMapping("/all-course")
    public List<Course> getAll() {
        return courseService.getAllCourse();
    }


    @PostMapping("/course-register")
    public ResponseEntity courseRegister(@RequestParam("courseid") String courseID,
                                         @RequestParam("coursename") String courseName,
                                         @RequestParam("coursedescription") String courseDescription,
                                         @RequestParam("courseterm") String courseTerm,
                                         @RequestParam("authkey") String authKey) {

        if (authenticationService.teacherExistByAuthKey(authKey)) { // if the teacher exists

            if (!courseService.courseExist(courseID)){
                try {
                    Teacher teacher = teacherService.getTeacherFromAuthKey(authKey);
                    Course course = new Course(courseID, courseName, courseDescription, courseTerm, teacher.getTeacherUUID());
                    courseService.saveCourse(course);

                    return ResponseEntity.ok("200");

                }catch (Exception e) {
                    return ResponseEntity.badRequest().body(e);
                }
            }
            return ResponseEntity.badRequest().body("Course "+courseID+" already exist");

        }
        return ResponseEntity.badRequest().body("Please log in");
    }

    @PostMapping("/edit-course")
    public ResponseEntity editCourse(@RequestParam("oldcourseid") String oldCourseID,
                                     @RequestParam("newcourseid") String newCourseID,
                                     @RequestParam("coursename") String courseName,
                                     @RequestParam("coursedescription") String courseDescription,
                                     @RequestParam("courseterm") String courseTerm,
                                     @RequestParam("authkey") String authKey) {

        if (authenticationService.teacherExistByAuthKey(authKey)) {

            if (courseService.courseExist(oldCourseID)){
                try {
                    Teacher teacher = teacherService.getTeacherFromAuthKey(authKey);
                    Course course = courseRepository.findByTeacherUUIDAndCourseID(teacher.getTeacherUUID(), oldCourseID);



                    courseService.editCourse(newCourseID, courseName, courseDescription, courseTerm, teacher, course);
                    return ResponseEntity.ok("200");


                }catch (Exception e) {
                    return ResponseEntity.badRequest().body(e.getMessage());
                }
            }
            return ResponseEntity.badRequest().body("Course "+oldCourseID+" does not exist");
        }
        return ResponseEntity.badRequest().body("Please log in");
    }

}
