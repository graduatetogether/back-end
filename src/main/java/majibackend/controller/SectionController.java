package majibackend.controller;


import majibackend.model.Course;
import majibackend.model.Section;
import majibackend.service.AuthenticationService;
import majibackend.service.CourseService;
import majibackend.service.SectionService;
import majibackend.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class SectionController {


    @Autowired
    private SectionService sectionService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping("/all-section")
    public Map<String, Object> getAll(){
        return sectionService.getAllSection();
    }

    @PostMapping("/edit-section")
    public ResponseEntity editSection(@RequestParam("authkey") String authKey,
                                      @RequestParam("newsectionnumber") Long newSectionNumber,
                                      @RequestParam("oldsectionnumber") Long oldSectionNumber,
                                      @RequestParam("sectionroom") String sectionRoom,
                                      @RequestParam("sectiondateandtime") String sectionDateAndTime,
                                      @RequestParam("courseid") String courseID) {

        // check if section number exist
        if (authenticationService.teacherExistByAuthKey(authKey)) {

            if (sectionService.courseExistByID(courseID)) {
                Course course = courseService.getCourseByCourseID(courseID);
                Long courseUUID = course.getCourseUUID();



                try {

                    Section section = sectionService.getSectionByCourseUUIDAndSectionNumber(courseUUID, oldSectionNumber);
                    return sectionService.editSection(sectionDateAndTime, oldSectionNumber, newSectionNumber, sectionRoom, section, course);

                } catch (Exception e) {
                    return ResponseEntity.badRequest().body(e.getMessage());

                }

            }

            return ResponseEntity.badRequest().body("Course ID does not exist");

        }
        return ResponseEntity.badRequest().body("Please log in");

    }
    @PostMapping("/section-register")
    public ResponseEntity sectionRegister(@RequestParam("sectiondateandtime") String sectionDateAndTime,
                                          @RequestParam("sectionnumber") Long sectionNumber,
                                          @RequestParam("sectionroom") String sectionRoom,
                                          @RequestParam("courseid") String courseID,
                                          @RequestParam("authkey") String authKey) {

        if (authenticationService.teacherExistByAuthKey(authKey)) { // if Teacher exist

            if (sectionService.courseExistByID(courseID)) { //  if courseID exists

                Course course = courseService.getCourseByCourseID(courseID);
                Long courseUUID = course.getCourseUUID();


                if (sectionService.courseAndSectionExist(courseUUID, sectionNumber)) {  // if section in course does not exist

                    try {
                        String enrollID = sectionService.generateEnrollID();

                        String color = sectionService.randomColor();

                        Section section = new Section(sectionDateAndTime, sectionNumber,enrollID, sectionRoom, courseUUID, color);

                        sectionService.saveSection(section);

                        return  ResponseEntity.ok("200");
                    }catch (Exception e){
                        return ResponseEntity.badRequest().body(e);
                    }
                }
                return ResponseEntity.badRequest().body("This section already exist");
            }
            return ResponseEntity.badRequest().body("Course ID does not exist");
        }
        return ResponseEntity.badRequest().body("Please log in");
    }

    @GetMapping("/get-enrollid")
    public ResponseEntity getEnrollID(@RequestParam("authkey") String authKey,
                                      @RequestParam("courseid") String courseID,
                                      @RequestParam("sectionnumber") Long sectionNumber) {

        if (authenticationService.teacherExistByAuthKey(authKey)) {
            String enrollID = sectionService.getEnrollID(courseID, sectionNumber);

            return ResponseEntity.ok(enrollID);


        }
        return ResponseEntity.badRequest().body("Please log in");
    }

    @GetMapping("/get-section-from-course")
    public ResponseEntity getSectionFromFromCourse(@RequestParam("courseid") String courseID,
                                                   @RequestParam("authkey") String authKey) {

        if (authenticationService.teacherExistByAuthKey(authKey)) {
            try {
                return sectionService.getAllSectionByCourseID(courseID);
            } catch (Exception e){
                return ResponseEntity.badRequest().body(e);
            }
        }

        return ResponseEntity.badRequest().body("Please log in");

    }



}
