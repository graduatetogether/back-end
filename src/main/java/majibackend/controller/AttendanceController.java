package majibackend.controller;


import majibackend.model.Attendance;
import majibackend.model.Student;
import majibackend.repository.AttendanceRepository;
import majibackend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.sql.Date;
import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class AttendanceController {
    @Autowired
    private AttendanceService attendanceService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private QrService qrService;

    @Autowired
    private TimeService timeService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private SectionService sectionService;

//    @GetMapping("/all-attendance")
//    public List<Attendance> getAll() {
//        return attendanceRepository.findAll();
//    }

    @GetMapping("/attendance-recent-date")
    public ResponseEntity getRecentAttendance(@RequestParam("courseID") String courseID,
                                              @RequestParam("sectionNumber") Long sectionNumber,
                                              @RequestParam("authkey") String authKey) {
        if (authenticationService.teacherExistByAuthKey(authKey)) {
            try {
                Date d = attendanceService.getRecentDateByCourseAndSection(courseID, sectionNumber);
                Map<String, Date> res = new HashMap<>();
                res.put("date", d);
                return ResponseEntity.ok(res);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Please log in");
        }
    }

    @GetMapping("/list-attendance-date")
    public ResponseEntity getDateListInSection(@RequestParam("courseID") String courseID,
                                               @RequestParam("sectionNumber") Long sectionNumber,
                                               @RequestParam("authkey") String authKey) {
        if (authenticationService.teacherExistByAuthKey(authKey)) {
            try {
                List<Date> ds = attendanceService.getDateByCourseAndSection(courseID, sectionNumber);
                List<Date> noDup = new ArrayList<>(new HashSet<>(ds));
                Map<String, List<Date>> res = new HashMap<>();
                java.util.Collections.sort(noDup);
                res.put("date", noDup);
                return ResponseEntity.ok(res);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        }
        else {
            return ResponseEntity.badRequest().body("Please log in");
        }
    }

    @GetMapping("/all-attendance-by-date")
    public ResponseEntity getAllAttendanceInSection(@RequestParam("courseID") String courseID,
                                                    @RequestParam("sectionNumber") Long sectionNumber,
                                                    @RequestParam("date") Date date,
                                                    @RequestParam("authkey") String authKey) {
        if (authenticationService.teacherExistByAuthKey(authKey)) {
            try {
                List<Map> att_lst = attendanceService.getDateByCourseAndSectionAndDate(courseID, sectionNumber, date);
                Map<String, List> res = new HashMap<>();
                res.put("attendance", att_lst);
                return ResponseEntity.ok(res);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        }
        else {
            return ResponseEntity.badRequest().body("Please log in");
        }
    }

    @GetMapping("/student-attendance")
    public ResponseEntity getAllStudentAttendanceBySection(@RequestParam("studentID") String studentID,
                                                           @RequestParam("courseID") String courseID,
                                                           @RequestParam("sectionNumber") Long sectionNumber,
                                                           @RequestParam("authkey") String authKey) {
        if (authenticationService.studentExistByAuthKey(authKey)) {
            try {
                Long studentUUID = studentService.getStudentUUIDByID(studentID);

                Long sectionUUID = sectionService.getSectionUUIDByCourseIDAndSectionNumber(courseID, sectionNumber);
                String room = sectionService.getSectionRoomByCourseIDAndSectionNumber(courseID, sectionNumber);

                List<Map> res = attendanceService.findAllByStudentUUIDAndSectionUUID(studentUUID, sectionUUID, room);

                return ResponseEntity.ok(res);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        }
        else {
            return ResponseEntity.badRequest().body("Please log in");
        }
    }

    @PostMapping("/start-generate-qr")
    public ResponseEntity startGenQR(@RequestParam("courseID") String courseID,
                                      @RequestParam("sectionNumber") Long sectionNumber,
                                      @RequestParam("authkey") String authKey) {
        if (authenticationService.teacherExistByAuthKey(authKey)) {
            try {
                timeService.startGenQR(courseID, sectionNumber);
                return ResponseEntity.ok("200");
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Please log in");
        }
    }

    @PostMapping("/close-generate-qr")
    public ResponseEntity closeGenQR(@RequestParam("courseID") String courseID,
                                     @RequestParam("sectionNumber") Long sectionNumber,
                                     @RequestParam("authkey") String authKey) {
        if (authenticationService.teacherExistByAuthKey(authKey)) {
            try {
                timeService.closeGenQR(courseID, sectionNumber);

                studentService.setStudentToAbsent(courseID, sectionNumber);




                return ResponseEntity.ok("200");
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Please log in");
        }
    }

    @PostMapping("/generate-qr-secret")
    public ResponseEntity getQRSecret(@RequestParam("courseID") String courseID,
                                      @RequestParam("sectionNumber") Long sectionNumber,
                                      @RequestParam("authkey") String authKey) {
        if (authenticationService.teacherExistByAuthKey(authKey)) {
            try {
                String concated = qrService.encryptForQR(courseID, sectionNumber);

                Map<String, String> res = new HashMap<>();
                res.put("secret", concated);

                return ResponseEntity.ok(res);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Please log in");
        }
    }

    @PostMapping("/check-attendance")
    public ResponseEntity checkAttendance(@RequestParam("studentID") String studentID,
                                          @RequestParam("time") Time time,
                                          @RequestParam("secret") String secret,
                                          @RequestParam("authkey") String authKey,
                                          @RequestParam("installationid") String installationID) {

        // check wa installationid duplicate or not
        // if not then save

        if (authenticationService.studentExistByAuthKey(authKey)) {


            try {
                Map<String, String> decrypted = qrService.decryptQR(secret);
                String courseID = decrypted.get("courseID");
                Long sectionNumber = Long.parseLong(decrypted.get("sectionNumber"));

                long millis = System.currentTimeMillis();
                Date date = new java.sql.Date(millis);
                String s_date = date.toString();

                Time converted_time = java.sql.Time.valueOf(decrypted.get("time"));

                if (s_date.equals(decrypted.get("date"))) {
                    if (timeService.calculateDiffTime(converted_time, time) < 20) {
                        Student s = studentService.getStudentByID(studentID);
                        if (s != null) {
                            Long studentUUID = s.getStudentUUID();
                            Long sectionUUID = sectionService.getSectionUUIDByCourseIDAndSectionNumber(courseID, sectionNumber);
                            String status = timeService.calculateStatus(sectionUUID, time);

                            if (!attendanceService.checkInstallationID(installationID, sectionUUID)) {
                                Attendance attendance = new Attendance(date, time, status, studentUUID, sectionUUID, installationID);
                                attendanceService.saveAttendance(attendance);
                            }
                            else {
                                return ResponseEntity.badRequest().body("This student already checked attendance !");

                            }

                        }
                    } else {return ResponseEntity.badRequest().body("Qr is no longer valid");}
                } else { return ResponseEntity.badRequest().body("starting date not equal to decrypted date");}

                Map<String, Object> res = new HashMap<>();
                res.put("courseID", courseID);
                res.put("sectionNumber", sectionNumber);

                return ResponseEntity.ok(res);
            }
            catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        }
        else {
            return ResponseEntity.badRequest().body("Please log in");
        }
    }

    @PostMapping("/edit-attendance")
    public ResponseEntity editAttendance(@RequestParam("courseID") String courseID,
                                         @RequestParam("sectionNumber") Long sectionNumber,
                                         @RequestParam("studentID") String studentID,
                                         @RequestParam("newStatus") String newStatus,
                                         @RequestParam("date") Date date,
                                         @RequestParam("authkey") String authKey) {
        if (authenticationService.studentExistByAuthKey(authKey)) {
            try {
                attendanceService.editAttendance(courseID, sectionNumber, studentID, date, newStatus);
                return ResponseEntity.ok("200");
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Please log in");
        }
    }

    @GetMapping("/get-attendance-summary")
    public ResponseEntity getAttendanceSummary(@RequestParam("courseid") String courseID,
                                               @RequestParam("sectionnumber") Long sectionNumber,
                                               @RequestParam("authkey") String autheKey) {

        if (authenticationService.teacherExistByAuthKey(autheKey)) {


            try {
                List<Map> summary = attendanceService.getAttendanceSummary(courseID, sectionNumber);

                return ResponseEntity.ok().body(summary);


            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());

            }
        }
        return ResponseEntity.badRequest().body("Please log in");
    }
}
