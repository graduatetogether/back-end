package majibackend.repository;


import majibackend.model.Course;
import majibackend.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

    @Override
    List<Course> findAll();

    Course findByCourseUUID(Long courseUUID);

    Course findByCourseID(String courseID);

    boolean existsByCourseID(String courseID);

    List<Course> findAllByTeacherUUID(Long teacherUUID);

    Course findByTeacherUUIDAndCourseID(Long teacherUUID, String courseID);

}
