package majibackend.repository;

import majibackend.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {

    @Override
    List<Teacher> findAll();

    Teacher findByTeacherEmail(String teacherEmail);

    Teacher findByTeacherUUID(Long teacherUUID);

    Teacher findByAuthKey(String authKey);


    boolean existsByAuthKey(String authKey);
}
