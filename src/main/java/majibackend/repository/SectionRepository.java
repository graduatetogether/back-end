package majibackend.repository;


import majibackend.model.Section;
import majibackend.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface SectionRepository extends JpaRepository<Section, Integer> {
    @Override
    List<Section> findAll();

    Section findByCourseUUIDAndSectionNumber(Long courseUUID, Long sectionNumber);

    boolean existsByCourseUUIDAndSectionNumber(Long courseUUID, Long sectionNumber);

    Section findBySectionUUID(Long sectionUUID);

    Section findByEnrollID(String enrollID);

    List<Section> findAllByCourseUUID(Long courseUUID);

    Set<Student> findAllBySectionUUID(Long sectionUUID);

}
