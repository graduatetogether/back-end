package majibackend.repository;

import majibackend.model.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;


@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Integer> {

    @Override
    List<Attendance> findAll();

    Attendance findByDateAndSectionUUIDAndStudentUUID(Date date, Long sectionUUID, Long studentUUID);

    List<Attendance> findAllByStudentUUIDAndSectionUUID(Long studentUUID, Long sectionUUID);

    @Query("SELECT date FROM Attendance where sectionuuid = :sectionUUID")
    List<Date> getDateListFromSection(@Param("sectionUUID") Long sectionUUID);

    List<Attendance> findAllByDateAndSectionUUID(Date date, Long sectionUUID);

    List<Attendance> findAllBySectionUUIDAndStudentUUID(Long sectionUUID, Long studentUUID);

    List<Attendance> findAllBySectionUUIDAndStudentUUIDAndStatus(Long sectionUUID, Long studenUUID, String status);

    boolean existsByDateAndSectionUUIDAndInstallationID(Date date, Long sectionUUID, String installationID);
}
