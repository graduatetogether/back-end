package majibackend.repository;

import majibackend.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;


@Repository
public interface StudentRepository <T, ID extends Serializable> extends JpaRepository<Student, Integer> {

    @Override
    List<Student> findAll();

    Student findByStudentID(String studentID);

    Student findByAuthKey(String authKey);

    boolean existsByAuthKey(String authKey);

    boolean existsById(ID studentUUID);

    Student findByStudentUUID(Long studentUUID);

}
