package majibackend.service;


import majibackend.model.*;
import majibackend.repository.CourseRepository;
import majibackend.repository.SectionRepository;
import majibackend.repository.StudentRepository;
import majibackend.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;
import java.util.*;

@Service("studentService")
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private AttendanceService attendanceService;

    public void saveStudent(Student student) {
        studentRepository.save(student);
    }

    public boolean studentUUIDValid(Long studentUUID) {
        System.out.println("in studentUUIDValid in studentService studentUUID: " + studentUUID);
        System.out.println(studentRepository.existsById(studentUUID));
        System.out.println("after get into db");
        return studentRepository.existsById(studentUUID);
    }

    public boolean checkPasswordMatch(Student student, String enteredPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        return passwordEncoder.matches(enteredPassword, student.getPassword());

    }

    public List<Student> getAllStudent(){
        return studentRepository.findAll();
    }

    public boolean studentExist(String studentID) {
        return studentRepository.findByStudentID(studentID) == null;
    }

    public Student getStudentByID(String studentID) {
        return studentRepository.findByStudentID(studentID);
    }

    public Long getStudentUUIDByID(String studentID) {
        return getStudentByID(studentID).getStudentUUID();
    }

    public Student getStudentByAuthKey(String authKey) {
        return studentRepository.findByAuthKey(authKey);
    }

    public Map<String, Object> getStudentInfo(String authKey) {
        Student student = studentRepository.findByAuthKey(authKey);

        Map<String, Object> map = new HashMap<>();
        map.put("studentID", student.getStudentID());
        map.put("studentFname", student.getStudentFname());
        map.put("studentLname", student.getStudentLname());
        map.put("authKey", student.getAuthKey());

        Set<Section> sections = student.getSections();

        Set<Object> newSection = new HashSet<>();

        for (Section sec : sections) {
            Map<String, Object> mapSec = new HashMap<>();
            mapSec.put("sectionDateAndTime", sec.getSectionDateAndTime());
            mapSec.put("sectionNumber", sec.getSectionNumber());
            mapSec.put("enrollID", sec.getEnrollID());
            mapSec.put("room", sec.getRoom());
            mapSec.put("color", sec.getColor());

            Course course = courseRepository.findByCourseUUID(sec.getCourseUUID());
            Teacher teacher = teacherRepository.findByTeacherUUID(course.getTeacherUUID());

            mapSec.put("courseID", course.getCourseID());
            mapSec.put("courseName", course.getCourseName());
            mapSec.put("courseDescription", course.getCourseDescription());
            mapSec.put("courseTerm", course.getCourseTerm());
            mapSec.put("teacherFname", teacher.getTeacherFname());
            mapSec.put("teacherLname", teacher.getTeacherLname());
            mapSec.put("teacherEmail", teacher.getTeacherEmail());

            newSection.add(mapSec);

        }

        map.put("sections", newSection);

        return map;
    }

    public String studentEnroll(Section section, Student student) {

        Set<Section> sections = student.getSections();


        Set<Student> students = section.getStudents();

        //check if student already register to this section
        for (Section each : sections) {
            if (section.getSectionUUID().equals(each.getCourseUUID())){
                return "Student has already registered to this course";
            }
        }

        students.add(student);
        sections.add(section);

        section.setStudents(students);

        student.setSections(sections);

        studentRepository.save(student);
        sectionRepository.save(section);




        return "200";

    }


    public void setStudentToAbsent(String courseID, Long sectionNumber) {

        long millis=System.currentTimeMillis();
        Date date = new java.sql.Date(millis);
        Time time = new java.sql.Time(millis);

        List<Map> att_lst = attendanceService.getDateByCourseAndSectionAndDate(courseID, sectionNumber, date);

        Set<String> allstudentsinday = new HashSet<>();

        for (Map map : att_lst) {
            allstudentsinday.add(map.get("studentID").toString());

        }
        Course course = courseRepository.findByCourseID(courseID);
        Section section = sectionRepository.findByCourseUUIDAndSectionNumber(course.getCourseUUID(), sectionNumber);


        Set<Student> students = section.getStudents();
        Set<String> allStudentid = new HashSet<>();
        for (Student std : students) {
            allStudentid.add(std.getStudentID());
        }

        allStudentid.removeAll(allstudentsinday);


        for (String a : allStudentid) {
            Student id = studentRepository.findByStudentID(a);
            Attendance attendance = new Attendance(date, time, "Absent", id.getStudentUUID(), section.getSectionUUID(),null);
            attendanceService.saveAttendance(attendance);

        }



    }
}
