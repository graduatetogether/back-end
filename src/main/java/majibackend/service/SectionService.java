package majibackend.service;


import majibackend.model.Course;
import majibackend.model.Section;
import majibackend.model.Student;
import majibackend.model.Teacher;
import majibackend.repository.CourseRepository;
import majibackend.repository.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;


@Service("sectionService")
public class SectionService {

    @Autowired
    SectionRepository sectionRepository;

    @Autowired
    CourseRepository courseRepository;

    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public Map<String, Object> getAllSection(){

        List<Section> sections = sectionRepository.findAll();
        Map<String, Object> map = new HashMap<>();
        for (Section s : sections) {
            map.put("color", s.getColor());
            map.put("enrollid", s.getEnrollID());
            map.put("sectiondateandtime", s.getSectionDateAndTime());
            map.put("sectionnumber", s.getSectionNumber());
            map.put("sectionroom", s.getRoom());

        }

        return map;
    }

    public String generateEnrollID() {

        Random random = new Random();
        StringBuilder builder = new StringBuilder(6);

        for (int i = 0; i < 6; i++) {
            builder.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
        }

        return builder.toString();

    }

    public void saveSection(Section section) {
        sectionRepository.save(section);
    }

    public boolean courseExistByID(String courseID) {
        return courseRepository.existsByCourseID(courseID);

    }

    public boolean courseAndSectionExist(Long courseUUID, Long sectionNumber) {
//        return sectionRepository.findByCourseUUIDAndSectionNumber(courseUUID, sectionNumber) == null;

        return !sectionRepository.existsByCourseUUIDAndSectionNumber(courseUUID, sectionNumber);
    }

    public Long getSectionUUIDByCourseIDAndSectionNumber(String courseID, Long sectionNumber) {
        Course c = courseRepository.findByCourseID(courseID);
        Long courseUUID = c.getCourseUUID();
        Section s = sectionRepository.findByCourseUUIDAndSectionNumber(courseUUID, sectionNumber);
        return s.getSectionUUID();
    }

    public String getSectionRoomByCourseIDAndSectionNumber(String courseID, Long sectionNumber) {
        Course c = courseRepository.findByCourseID(courseID);
        Long courseUUID = c.getCourseUUID();
        Section s = sectionRepository.findByCourseUUIDAndSectionNumber(courseUUID, sectionNumber);
        return s.getRoom();
    }

    public String randomColor() {
        Random random = new Random();

        // create a big random number - maximum is ffffff (hex) = 16777215 (dez)
        int nextInt = random.nextInt(0xffffff + 1);
        // format it as hexadecimal string (with hashtag and leading zeros)
        String colorCode = String.format("#%06x", nextInt);
        return colorCode;
    }

    public String getEnrollID(String courseID, Long sectionNumber) {

        Course course = courseRepository.findByCourseID(courseID);

        Section section = sectionRepository.findByCourseUUIDAndSectionNumber(course.getCourseUUID(), sectionNumber);

        return section.getEnrollID();
    }

    public Section findByCourseUUIDAndSectionNumber(Long courseUUID, Long sectionNumber) {
        return sectionRepository.findByCourseUUIDAndSectionNumber(courseUUID, sectionNumber);
    }

    public Section getSectionByEnrollID(String enrollID) {
        return sectionRepository.findByEnrollID(enrollID);
    }

    public ResponseEntity getAllSectionByCourseID(String courseID) {
        if (courseRepository.existsByCourseID(courseID)) {
            Course course = courseRepository.findByCourseID(courseID);

            List<Section> sections = sectionRepository.findAllByCourseUUID(course.getCourseUUID());


            List<Map> map2 = new ArrayList<>();

            for (Section s : sections) {

                Map<String, Object> map = new HashMap<>();
                map.put("sectionNumber", s.getSectionNumber());
                map.put("color", s.getColor());
                map.put("enrollID", s.getEnrollID());
                map.put("sectionDateAndTime", s.getSectionDateAndTime());
                map.put("sectionroom", s.getRoom());
                map.put("sectionUUID", s.getSectionUUID());

                map2.add(map);
            }



            return ResponseEntity.ok().body(map2);
        }
        return ResponseEntity.badRequest().body("Course "+courseID+" does not exist");
    }

    public Section findBySectionUUID(Long sectionUUID) {
        return sectionRepository.findBySectionUUID(sectionUUID);
    }



    public ResponseEntity editSection(String sectionDateAndTime, Long oldSectionNumber,Long newSectionNumber, String sectionRoom, Section section, Course course) {

        if (oldSectionNumber == newSectionNumber || !sectionRepository.existsByCourseUUIDAndSectionNumber(course.getCourseUUID(), newSectionNumber)) {

            section.setSectionDateAndTime(sectionDateAndTime);
            section.setSectionNumber(newSectionNumber);
            section.setRoom(sectionRoom);

            sectionRepository.save(section);

            return ResponseEntity.ok("200");

        }
//        if (!sectionRepository.existsByCourseUUIDAndSectionNumber(course.getCourseUUID(), newSectionNumber)) {
//            section.setSectionDateAndTime(sectionDateAndTime);
//            section.setSectionNumber(newSectionNumber);
//            section.setRoom(sectionRoom);
//
//            sectionRepository.save(section);
//
//            return ResponseEntity.ok("200");
//
//        }
        return ResponseEntity.badRequest().body("section " + newSectionNumber.toString() + "already exists");



    }

    public Section getSectionByCourseUUIDAndSectionNumber(Long courseUUID, Long sectionNumber) {
        return sectionRepository.findByCourseUUIDAndSectionNumber(courseUUID, sectionNumber);

    }




}
