package majibackend.service;


import majibackend.model.Course;
import majibackend.model.Section;
import majibackend.model.Teacher;
import majibackend.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("courseService")
public class CourseService {

    @Autowired
    CourseRepository courseRepository;

    public List<Course> getAllCourse(){
        return courseRepository.findAll();
    }

    public void saveCourse(Course course) {
        courseRepository.save(course);
    }

    public Long getCourseUUIDByCourseID(String courseID) {
        Course c = courseRepository.findByCourseID(courseID);
        return c.getCourseUUID();
    }

    public boolean courseExist(String courseID) {
        return courseRepository.existsByCourseID(courseID);
    }

    public Course getCourseByCourseID(String courseID) {
        return courseRepository.findByCourseID(courseID);
    }

    public void editCourse(String newCourseID, String courseName, String courseDescription, String courseTerm, Teacher teacher, Course course) {

        course.setCourseID(newCourseID);
        course.setCourseName(courseName);
        course.setCourseDescriotion(courseDescription);
        course.setCourseTerm(courseTerm);

        courseRepository.save(course);
    }

}
