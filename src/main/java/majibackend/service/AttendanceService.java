package majibackend.service;


import majibackend.model.Attendance;
import majibackend.model.Course;
import majibackend.model.Section;
import majibackend.model.Student;
import majibackend.repository.AttendanceRepository;
import majibackend.repository.CourseRepository;
import majibackend.repository.SectionRepository;
import majibackend.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;
import java.util.*;

@Service("attendanceService")
public class AttendanceService {
    @Autowired
    private StudentService studentService;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private AttendanceRepository attendanceRepository;
    
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private SectionRepository sectionRepository;

    public void saveAttendance(Attendance attendance) {
        Long studentUUID = attendance.getStudentUUID();
        Long sectionUUID = attendance.getSectionUUID();
        Date date = attendance.getDate();
        try {
            studentUUIDValid(studentUUID);
            if (!dateAndSectionNotChecked(date, sectionUUID, studentUUID)) {
                throw new IllegalArgumentException("already check attendance");
            }
            attendanceRepository.save(attendance);
        }
        catch (IllegalArgumentException e) {
            throw e;
        }
    }

    public boolean studentUUIDValid(Long studentUUID) {
        if (studentService.studentUUIDValid(studentUUID)) {
            return true;
        }
        else {
            throw new IllegalArgumentException("student does not exit");
        }
    }

    public boolean dateAndSectionNotChecked(Date date, Long sectionUUID, Long studentUUID) {
        return attendanceRepository.findByDateAndSectionUUIDAndStudentUUID(date, sectionUUID, studentUUID) == null;
    }

    public void editAttendance(String courseID, Long sectionNumber, String studentID, Date date, String newStatus) {
        try {
            // get SectionUUID
            Long sectionUUID = sectionService.getSectionUUIDByCourseIDAndSectionNumber(courseID, sectionNumber);

            // get StudentUUID
            Long studentUUID = studentService.getStudentUUIDByID(studentID);

            // edit attendance in the table
            Attendance a = attendanceRepository.findByDateAndSectionUUIDAndStudentUUID(date, sectionUUID, studentUUID);
            a.setStatus(newStatus);
            attendanceRepository.save(a);
        }
        catch (IllegalArgumentException e) {
            throw e;
        }
    }

    public List<Date> getDateByCourseAndSection(String courseID, Long sectionNumber) {
        try {
            Long sectionUUID = sectionService.getSectionUUIDByCourseIDAndSectionNumber(courseID, sectionNumber);
            return attendanceRepository.getDateListFromSection(sectionUUID);
        } catch (Exception e) {
            throw e;
        }
    }

    public Date getRecentDateByCourseAndSection(String courseID, Long sectionNumber) {
        try {
            Long sectionUUID = sectionService.getSectionUUIDByCourseIDAndSectionNumber(courseID, sectionNumber);
            List<Date> d_lst = attendanceRepository.getDateListFromSection(sectionUUID);
            java.util.Collections.sort(d_lst);
            Integer d_size = d_lst.size();
            if (d_size > 0) {
                return d_lst.get(d_lst.size() - 1);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Map> getDateByCourseAndSectionAndDate(String courseID, Long sectionNumber, Date date) {
        try {
            List<Map> res = new ArrayList<>();
            Long sectionUUID = sectionService.getSectionUUIDByCourseIDAndSectionNumber(courseID, sectionNumber);
            List<Attendance> as = attendanceRepository.findAllByDateAndSectionUUID(date, sectionUUID);

            for (Attendance a: as) {
                System.out.println("a");
                Map<String, Object> each = new HashMap<>();
                
                Long studentUUID = a.getStudentUUID();
                Student s = studentRepository.findByStudentUUID(studentUUID);
                each.put("studentID", s.getStudentID());
                each.put("student_fname", s.getStudentFname());
                each.put("student_lname", s.getStudentLname());

                each.put("time", a.getTime());
                each.put("status", a.getStatus());

                res.add(each);
            }
            return res;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Map> findAllByStudentUUIDAndSectionUUID(Long studentUUID, Long sectionUUID, String room) {
        List<Map> ret = new ArrayList<>();
        List<Attendance> att = attendanceRepository.findAllByStudentUUIDAndSectionUUID(studentUUID, sectionUUID);

        for (Attendance a: att) {
            Map<String, Object> tmp = new HashMap<>();

            Date date = a.getDate();
            Time time = a.getTime();
            String status = a.getStatus();

            tmp.put("date", date);
            tmp.put("time", time);
            tmp.put("status", status);
            tmp.put("room", room);

            ret.add(tmp);
        }
        return ret;
    }

    public List<Map> getAttendanceSummary(String courseID, Long sectionNumber) {

        Course course = courseRepository.findByCourseID(courseID);

        Section section = sectionRepository.findByCourseUUIDAndSectionNumber(course.getCourseUUID(), sectionNumber);

        Long sectionUUID = section.getSectionUUID();

        Set<Student> students = section.getStudents();

        List<Map> allSummary = new ArrayList<>();

        for (Student std : students) {
            Map<String, Object> stdSummary = new HashMap<>();
            Long stdUUID = std.getStudentUUID();

            List<Attendance> present = attendanceRepository.findAllBySectionUUIDAndStudentUUIDAndStatus(sectionUUID, stdUUID, "Present");
            List<Attendance> late = attendanceRepository.findAllBySectionUUIDAndStudentUUIDAndStatus(sectionUUID, stdUUID, "Late");
            List<Attendance> absent = attendanceRepository.findAllBySectionUUIDAndStudentUUIDAndStatus(sectionUUID, stdUUID, "Absent");

            stdSummary.put("studentID", std.getStudentID());
            stdSummary.put("studentFname", std.getStudentFname());
            stdSummary.put("studentLname", std.getStudentLname());
            stdSummary.put("present", present.size());
            stdSummary.put("late", late.size());
            stdSummary.put("absent", absent.size());

            allSummary.add(stdSummary);

        }
        return allSummary;

    }

    public boolean checkInstallationID(String installationID, Long sectionUUID) {

        long millis=System.currentTimeMillis();
        Date date = new java.sql.Date(millis);

        boolean attd = attendanceRepository.existsByDateAndSectionUUIDAndInstallationID(date, sectionUUID, installationID);

        return attd;



    }

}
