package majibackend.service;


import majibackend.repository.StudentRepository;
import majibackend.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("authenticationService")
public class AuthenticationService {

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    StudentRepository studentRepository;


    public boolean teacherExistByAuthKey(String authKey) {
        return teacherRepository.existsByAuthKey(authKey);
    }

    public boolean studentExistByAuthKey(String authKey) {

        return studentRepository.existsByAuthKey(authKey);
    }
}
