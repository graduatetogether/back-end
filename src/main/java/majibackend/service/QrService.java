package majibackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("qrService")
public class QrService {
    @Autowired
    private AesService aesService;

    public String encryptForQR(String courseID, Long sectionNumber) {
        try {
            long millis=System.currentTimeMillis();
            Date date = new java.sql.Date(millis);
            Time time = new java.sql.Time(millis);

            String encrypted_courseID = aesService.encrypt(courseID);
            String encrypted_sectionNumber = aesService.encrypt(sectionNumber.toString());
            String encrypted_date = aesService.encrypt(date.toString());
            String encrypted_time = aesService.encrypt(time.toString());

            return encrypted_courseID.concat(encrypted_sectionNumber).concat(encrypted_date).concat(encrypted_time);
        } catch (Exception e) {
            System.out.println("Error while encrypting for qr: " + e.toString());
            throw new IllegalArgumentException("Error while encrypting for qr: " + e.toString());
        }
    }

    public Map<String, String> decryptQR(String secret) {
        try {
            String[] splited = secret.replace(" ", "+").split("==");

            String encrypted_courseID = splited[0] + "==";
            String encrypted_sectionNumber = splited[1] + "==";
            String encrypted_date = splited[2] + "==";
            String encrypted_time = splited[3] + "==";

            String decrypted_courseID = aesService.decrypt(encrypted_courseID);
            String decrypted_sectionNumber = aesService.decrypt(encrypted_sectionNumber);
            String decrypted_date = aesService.decrypt(encrypted_date);
            String decrypted_time = aesService.decrypt(encrypted_time);

            Map<String, String> decrypted = new HashMap<>();
            decrypted.put("courseID", decrypted_courseID);
            decrypted.put("sectionNumber", decrypted_sectionNumber);
            decrypted.put("date", decrypted_date);
            decrypted.put("time", decrypted_time);


            return decrypted;
        } catch (Exception e) {
            System.out.println("Error while decrypting for QR: " + e.toString());
            throw new IllegalArgumentException("Error while decrypting for QR: " + e.toString());
        }
    }
}
