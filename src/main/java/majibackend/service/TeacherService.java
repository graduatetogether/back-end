package majibackend.service;


import majibackend.model.Course;
import majibackend.model.Teacher;
import majibackend.repository.CourseRepository;
import majibackend.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("teacherService")
public class TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private CourseRepository courseRepository;


    public void saveTeacher(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public boolean checkPasswordMatch(Teacher teacher, String enteredPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        return passwordEncoder.matches(enteredPassword, teacher.getPassword());

    }

    public boolean teacherExistByEmail(String teacherEmail) {
        return teacherRepository.findByTeacherEmail(teacherEmail) == null;

    }

//    public boolean teacherExistByAuthKey(String authKey) {
//        return teacherRepository.findByAuthKey(authKey) != null;
//    }

    public Teacher getTeacherFromAuthKey(String authKey) {
        return teacherRepository.findByAuthKey(authKey);

    }


    public Teacher getTeacherFromEmail(String teacherEmail) {
        return teacherRepository.findByTeacherEmail(teacherEmail);
    }

    public List<Teacher> getAllTeacher(){
        return teacherRepository.findAll();
    }

    public List<Course> getAllCourseByTeacher(String authKey) {
        Teacher teacher = teacherRepository.findByAuthKey(authKey);

        return courseRepository.findAllByTeacherUUID(teacher.getTeacherUUID());

    }




}
