package majibackend.service;

import majibackend.model.Section;
import majibackend.repository.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Service("timeService")
public class TimeService {
    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private CourseService courseService;

    public long calculateDiffTime(Time after, Time before) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

            LocalTime dateTime1= LocalTime.parse(after.toString(), formatter);
            LocalTime dateTime2= LocalTime.parse(before.toString(), formatter);

            long diffInSeconds = java.time.Duration.between(dateTime1, dateTime2).getSeconds();

            return diffInSeconds;
        } catch (Exception e) {
            throw new IllegalArgumentException("Error while calculating time: " + e.toString());
        }
    }

    public void startGenQR(String courseID, Long sectionNumber) {
        try {
            long millis = System.currentTimeMillis();
            Date date = new java.sql.Date(millis);
            Time time = new java.sql.Time(millis);

            Long courseUUID = courseService.getCourseUUIDByCourseID(courseID);
            Section s = sectionService.findByCourseUUIDAndSectionNumber(courseUUID, sectionNumber);
            s.setStartGenQRDate(date);
            s.setStartGenQRTime(time);

            sectionRepository.save(s);
        } catch (Exception e) {
            throw new IllegalArgumentException("Error while calculating time: " + e.toString());
        }
    }

    public void closeGenQR(String courseID, Long sectionNumber) {
        try {
            Long courseUUID = courseService.getCourseUUIDByCourseID(courseID);
            Section s = sectionService.findByCourseUUIDAndSectionNumber(courseUUID, sectionNumber);
            s.setStartGenQRTime(null);

            sectionRepository.save(s);
        } catch (Exception e) {
            throw new IllegalArgumentException("Error while calculating time: " + e.toString());
        }
    }

    public String calculateStatus(Long sectionUUID, Time checkin) {
        try {
            Section s = sectionService.findBySectionUUID(sectionUUID);
            Time start = s.getStartGenQRTime();
            long diff = calculateDiffTime(start, checkin);

            if (diff < 900) {
                return "Present";
            }
            return "Late";
        } catch (Exception e) {
            throw new IllegalArgumentException("Error while calculating status: " + e.toString());
        }
    }
}
