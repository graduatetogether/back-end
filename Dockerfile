FROM openjdk:8
ADD target/maji-backend.jar maji-backend.jar
ADD src/main/resources/application.properties application.properties
EXPOSE 8086
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "maji-backend.jar"]